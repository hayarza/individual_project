<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

JUST THE AGENT CLASS... PRETTY STANDARD STUFF

 -->

<?php
  class Agent {

    //----CLASS OPTIONS----//

    private $AgentId;
    private $AgtFirstName;
    private $AgtMiddleInitial;
    private $AgtLastName;
    private $AgtEmail;
    private $AgtBusPhone;
    private $AgtPosition;
    private $AgencyId;

    //----CLASS CONSTRUCTORS----//

    public function __construct($data) {
      $values = array_values($data);

      // array_shift($values); only if there's a mode field inside forms!

      $this->AgentId = $values[0];
      $this->AgtFirstName = $values[1];
      $this->AgtMiddleInitial= $values[2];
      $this->AgtLastName = $values[3];
      $this->AgtBusPhone = $values[4];
      $this->AgtEmail = $values[5];
      $this->AgtPosition = $values[6];
      $this->AgencyId = $values[7];
    }

    //----CLASS METHODS----//
      //--SETTERS AND GETTERS--//
    public function getAgentId() {
      return $this->AgentId;
    }

    public function setAgentId($data) {
      $this->AgentId = $data;
    }

    public function getAgtFirstName() {
      return $this->AgtFirstName;
    }

    public function setAgtFirstName($data) {
      $this->AgtFirstName = $data;
    }

    public function getAgtMiddleInitial() {
      return $this->AgtMiddleInitial;
    }

    public function setAgtMiddleInitial($data) {
      $this->AgtMiddleInitial = $data;
    }

    public function getAgtLastName() {
      return $this->AgtLastName;
    }

    public function setAgtLastName($data) {
      $this->AgtLastName = $data;
    }

    public function getAgtEmail() {
      return $this->AgtEmail;
    }

    public function setAgtEmail($data) {
      $this->AgtEmail = $data;
    }

    public function getAgtBusPhone() {
      return $this->AgtBusPhone;
    }

    public function setAgtBusPhone($data) {
      $this->AgtBusPhone = $data;
    }

    public function getAgtPosition() {
      return $this->AgtPosition;
    }

    public function setAgtPosition($data) {
      $this->AgtPosition = $data;
    }

    public function getAgencyId() {
      return $this->AgencyId;
    }

    public function setAgencyId($data) {
      $this->AgencyId = $data;
    }

    //---UTILITY---//

      //--ARRAY FOR updateAgent();--//
    public function updateArray() {
      $array[] = $this->getAgtFirstName();
      $array[] = $this->getAgtMiddleInitial();
      $array[] = $this->getAgtLastName();
      $array[] = $this->getAgtBusPhone();
      $array[] = $this->getAgtEmail();
      $array[] = $this->getAgtPosition();
      $array[] = $this->getAgencyId();
      $array[] = $this->getAgentId(); //placed here to fit the database table
      return $array;

    }

      //--ARRAY FOR createAgent();--//
    public function createArray() {
      $array[] = $this->getAgtFirstName();
      $array[] = $this->getAgtMiddleInitial();
      $array[] = $this->getAgtLastName();
      $array[] = $this->getAgtBusPhone();
      $array[] = $this->getAgtEmail();
      $array[] = $this->getAgtPosition();
      $array[] = $this->getAgencyId();
      return $array;
    }

      //--ARRAY FOR DELETE--//
    public function deleteArray() {
      $array[] = $this->getAgentId();
      return $array;
    }



  }
 ?>
