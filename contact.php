<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

PROOF OF CONCEPT FORM FOR CONTACTING THE COMPANY

MISSING CAPTCHA lol

 -->
<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Contact Us</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php include_once("navbar.php"); ?>
    <h1 style="text-align:center;">Contact Form</h1>

<div class="display_form">
  <form action="bouncer.php">
    <label for="fname">First Name</label>
    <input type="text" id="fname" name="firstname" placeholder="Your First Name...">

    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your Last Name...">

    <label for="country">Country</label>
    <select id="country" name="country">
      <option value="Canada">Canada</option>
      <option value="Venezuela">Venezuela</option>
      <option value="usa">USA</option>
    </select>

    <label for="subject" style="margin-top: 10px;">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px;"></textarea>
    <br />
    <button class="cssbutton">SUBMIT</button>
  </form>
</div>

  </body>
  <?php include_once("footer.php"); ?>
