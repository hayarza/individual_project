<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

THIS FORM INSERTS AGENTS INTO DATABASE!

 -->
<?php
  session_start();

  if ($_SESSION['loggedin'] == false) {
    header("Location: login.php");
    $_SESSION['returnpage'] = "create_form.php";
    $_SESSION["message"] = "You need to log in to view this page";
  }
 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Agent Create Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php include_once("navbar.php"); ?>
    <h1 style="text-align:center;">AGENT CREATE FORM</h1>

    <div class="display_form">

    <form method="post" action="insertagent.php">
      <label for="AgtFirstName">First Name:</label>
      <input type="text" name="AgtFirstName" id="AgtFirstName" class="input" /> <br />
      <label for="AgtMiddleInitial">Middle Name Initial</label>
      <input type="text" name="AgtMiddleInitial" id="AgtMiddleInitial" class="input" /> <br />
      <label for="AgtLastName">Last Name:</label>
      <input type="text" name="AgtLastName" id="AgtLastName" class="input" /> <br />
      <label for="AgtBusPhone">Agent Phone Number:</label>
      <input type="text" name="AgtBusPhone" id="AgtBusPhone" class="input" /> <br />
      <label for="AgtEmail">Agent Email:</label>
      <input type="text" name="AgtEmail" id="AgtEmail" class="input" /> <br />
      <label for="AgtPosition">Agent Position:</label>
      <input type="text" name="AgtPosition" id="AgtPosition" class="input" /> <br />
      <label for="AgencyId">Agency ID:</label>
      <input type="text" name="AgencyId" id="AgencyId" class="input" /> <br />
      <div style="text-align:center;">
        <button class="cssbutton">Register</button>
        <button onclick="return confirm('Are you sure you want to reset the form?');" type="reset" class="cssbutton">Reset</button>
      </div>

    </form>

    </div>

  </body>
<?php include_once("footer.php"); ?>
