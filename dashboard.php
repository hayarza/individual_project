<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

SERVES AS A HUB FOR DIFFERENT FORMS!

 -->
<?php
  session_start();

  if ($_SESSION['loggedin'] == false) {
    header("Location: login.php");
    $_SESSION['returnpage'] = "dashboard.php";
    $_SESSION["message"] = "You need to log in to view this page";
  }
 ?>
 <!DOCTYPE html>
 <html>
  <head>
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php include_once("navbar.php"); ?>
    <div class="display">
      <div class="dlogo" style="padding-bottom: 50px;"><img src="images/icon.png" /></div>
      <div class="button_row">
        <button class="cssbutton" onclick="window.location.href='create_form.php';">CREATE</button>
        <button class="cssbutton" onclick="window.location.href='update_form.php';">UPDATE</button>
        <button class="cssbutton" onclick="window.location.href='update_form.php';">DELETE</button>
      </div>

    </div>

  </body>
    <?php include_once("footer.php"); ?>
