<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS
DELETE AGENT ACTION SCRIPT
-->

<!DOCTYPE html>
<html>
  <head>
    <title>Agent Delete</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php
      include_once("navbar.php");
      include_once("variables.php");
      include_once("functions.php");
      include_once("Agent.php");

      if (!isset($_REQUEST['AgentId'])) {
        echo "NO DATA PROVIDED";
        exit;
      } else {
        $agent = new Agent($_REQUEST);
        if(!deleteAgent($agent)) {
          echo "SOMETHING WENT WRONG";
          exit;
        }
        deleteAgent($agent);
        echo "<h1>SUCCESS! You have DELETED the Agent Record</h1>";
      }



      ?>

  </body>
<?php include_once("footer.php"); ?>
