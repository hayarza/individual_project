<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS -->
<?php
  include_once("variables.php");

//------ DATABASE CONNECT ---------//

  function dbConnect()
  {
      global $host, $user, $u_pwd, $db;
      $dbh = mysqli_connect($host, $user, $u_pwd, $db);
      if (!$dbh)
      {
        echo "Connection Failed: " . mysqli_connect_error() . "<br />";
        echo "Connection Error Number: " . mysqli_connect_errno() . "<br />";
        exit;
      }
      return $dbh;

  }

  //--DATABASE CHANGES--//

  function dbChange($sql, $values, $typestring) {
    $dbh = dbConnect();
    $statement = mysqli_prepare($dbh, $sql);
    mysqli_stmt_bind_param($statement, $typestring, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5], $values[6], $values[7]);
    $result = mysqli_stmt_execute($statement);

    if (!$result)
      {
        echo "DB CHANGE FAILED";
        mysqli_close($dbh);
        return false;
      }
      mysqli_close($dbh);
      return true;
  }

  //--AGENT CREATE (INSERT)--//

  //Had to repeat most of dbChange() because: mysqli_stmt_bind_param(): Number of elements in type definition string doesn't match number of bind variable -
  //will revisit this code to make more generic, I'm aware that this is a temporary workaround.
  //There's a weird bug thats producing 2 table inserts... cant' figure out why.


  function createAgent($agent) {
    $values = $agent->createArray();
    $sql = "INSERT INTO agents (AgtFirstName, AgtMiddleInitial, AgtLastName, AgtBusPhone, AgtEmail, AgtPosition, AgencyId) VALUES (?, ?, ?, ?, ?, ?, ?)";
    // return dbChange($sql, $values, "ssssssi");
    $dbh = dbConnect();
    $statement = mysqli_prepare($dbh, $sql);
    mysqli_stmt_bind_param($statement, 'ssssssi', $values[0], $values[1], $values[2], $values[3], $values[4], $values[5], $values[6]);
    $result = mysqli_stmt_execute($statement);

    if (!$result) {
      echo "DB CHANGE FAILED";
      mysqli_close($dbh);
      return false;
    }
    mysqli_close($dbh);
    return true;

}

  //--AGENT DELETE--//

  function deleteAgent($agent) {
    $values[] = $agent->getAgentId();
    $sql = "DELETE FROM agents WHERE AgentId=?";
    // return dbChange($sql, $values, "i");
    $dbh = dbConnect();
    $statement = mysqli_prepare($dbh, $sql);
    mysqli_stmt_bind_param($statement, 'i', $values[0]);
    $result = mysqli_stmt_execute($statement);

    if (!$result) {
      echo "DB CHANGE FAILED";
      mysqli_close($dbh);
      return false;
    }
    mysqli_close($dbh);
    return true;
  }


  //--AGENT UPDATE--//

  function updateAgent($agent) {
    $values = $agent->updateArray();
    $sql = "UPDATE agents SET AgtFirstName=?, AgtMiddleInitial=?, AgtLastName=?, AgtBusPhone=?, AgtEmail=?, AgtPosition=?, AgencyId=? WHERE AgentId=?";
    return dbChange($sql, $values, "ssssssii");

  }
?>
