<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS -->
<?php
$hours = date("G");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Travel Agency</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />

  </head>


  <body>
    <?php
    include_once("navbar.php");
  //----Dynamic Greetings Message------//
    if ($hours >=0 and $hours <12) {
      echo "<div class='greetings'>
      Good Morning
      </div>";
    } elseif ($hours >=12 and $hours < 18) {
      echo "<div class='greetings'>
      Good Afternoon
      </div>";
    } else {
      echo "<div class='greetings'>
      Good Evening
      </div>";
    }
     ?>


    <div class="display_main">
      <h1 id="main_header">TRAVEL PACKAGES</h1>
      <div class="imagesrow">
        <div class="imgcontainer1"></div>
        <div class="imgcontainer2"></div>
        <div class="imgcontainer3"></div>
      </div>

    </div>

  </body>

  <?php
    include_once("footer.php");
   ?>
