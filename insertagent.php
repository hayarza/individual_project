<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS -->

<!DOCTYPE html>
<html>
  <head>
    <title>Create Agent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php
      include_once("navbar.php");
      include_once("variables.php");
      include_once("functions.php");
      include_once("Agent.php");

      if (!isset($_REQUEST)) {
        echo "NO DATA PROVIDED";
        exit;
      } else {
        $array = array_values($_REQUEST);
        array_unshift($array, "NULL");
        $agent = new Agent($array);
          if (!createAgent($agent)) {
          echo "SOMETHING WENT WRONG";
          exit;
        } else {
          createAgent($agent);
          unset($_REQUEST);
          echo "<h1>You have Successfully Created an Agent Record</h1>";
          }
      }



     ?>
  </body>

    <?php include_once("footer.php"); ?>
