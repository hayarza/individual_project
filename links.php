<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

COULDNT FIGURE OUT WHAT TO DO WITH THIS AREA :D 

-->
<?php
  session_start();

  if ($_SESSION['loggedin'] == false) {
    header("Location: login.php");
    $_SESSION['returnpage'] = "links.php";
    $_SESSION["message"] = "You need to log in to view this page";
  }
 ?>
<!DOCTYPE html>
<html>
  <head>
  <?php include_once("variables.php"); ?>
    <title>Links</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
  <?php include_once("navbar.php"); ?>


  </body>
  <?php include_once("footer.php"); ?>
