<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS -->
<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login to your Account</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
  </head>
  <body>
    <?php include_once("navbar.php"); ?>
    <h1 style="text-align: center;">Log In Form</h1>
    <div class="display_form">

    <form method="get" action="verifylogin.php">
      USERNAME:<input type="text" name="AgtUserName" /><br />
      PASSWORD:<input type="text" name="AgtPwd" /><br />
      <div style="text-align:center;"><button class="cssbutton">Log In</button></div>
      <p>
        <?php
        if (isset($_SESSION["message"])) {
          print($_SESSION["message"]);
          unset($_SESSION["message"]);
        } elseif (isset($_SESSION['loggedin'])) {
          header("Location: dashboard.php");
        }
         ?>
      </p>
    </form>


  </div>
  </body>
<?php include_once("footer.php"); ?>
