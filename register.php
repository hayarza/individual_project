<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS
this form is mainly just to test JS Form Validation, haven't given it any other use yet!
 -->
<!DOCTYPE html>
<html>
  <head>
    <title>Register</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
    <script src="script.js" type="text/javascript"></script>
  </head>
  <body>
    <?php
    include_once("navbar.php");
     ?>
     <h1 style="text-align:center;">REGISTER FORM</h1>
     <div class="display_form">

     <form method="get" action="bouncer.php">
       <label for="fName">First Name:</label>
       <input type="text" name="fName" id="fName" class="input" /> <br />
       <label for="lName">Last Name:</label>
       <input type="text" name="lName" id="lName" class="input" /> <br />
       <label for="email">E-Mail:</label>
       <input type="email" name="email" id="email" class="input" /> <br />
       <label for="address">Address:</label>
       <input type="text" name="address" id="address" class="input" /> <br />
       <label for="city">City:</label>
       <input type="text" name="city" id="city" class="input" /> <br />
       <label for="prov">Province:</label>
       <input type="text" name="prov" id="prov" class="input" /> <br />
       <label for="post">Postal Code:</label>
       <input type="text" name="post" id="post" class="input" /> <br />
        <div style="text-align:center;">
         <button onclick="return validate();" class="cssbutton">Register</button>
         <button onclick="return confirm('Are you sure you want to reset the form?');" type="reset" class="cssbutton">Reset</button>
        </div>
     </form>

     </div>

  </body>
  <?php
    include_once("footer.php");
   ?>
