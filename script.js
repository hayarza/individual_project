 // Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS

// function changeColorInput(id) {
//   x = document.getElementById(id);
//   x.style.backgroundColor = 'red';
// }
//
// function show(id) {
//   x = document.getElementsById(id);
//   buttons = document.getElementsByTagName('button');
//   if (x.style.display === "none") {
//     x.style.display === "flex";
//     buttons.style.display === "none";
//   }
// }

// FORM INTERACTION

function changeAction() {
  var form = document.getElementById('updateform');
  form.action = "delete_agent.php";
}
// FORM VALIDATION

function validate() {
  var postex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/i; //regex for postal code
  var input = document.getElementsByClassName('input');

      if (input[0].value === "") {
        alert("Input your First Name");
        input[0].focus();
        return false;
      }
      if (input[1].value === "") {
        alert("Input your Last Name");
        input[1].focus();
        return false;
      }
      if (input[2].value === "") {
        alert("Input your email");
        input[2].focus();
        return false;
      }
      if (input[3].value === "") {
        alert("Input your Address");
        input[3].focus();
        return false;
      }
      if (input[4].value === "") {
        alert("Input your City");
        input[4].focus();
        return false;
      }
      if (input[5].value === "") {
        alert("Input your Province");
        input[5].focus();
        return false;
      }
      if (input[6].value === "") {
        alert("Input your Postal Code");
        input[6].focus();
        return false;
      } else if (!postex.test(input[6].value)) {
        alert("Enter Valid Postal Code");
        return false;
      }

      return confirm("Continue Submitting?");

}
