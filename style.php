/*Created by: Hernan Ayarza
php was added to the stylesheet to make it dynamic... but decided not use it in the end because it looked rather ugly.
*/
<?php
header("Content-type: text/css");
$hours = date("G");
$background = "url(images/morning.jpg);";
if ($hours >=0 and $hours <12) {
  $background = "url(images/morning.jpg);";
} elseif ($hours >=12 and $hours < 18) {
  $background = "url(images/afternoon.jpg);";
} else {
  $background = "url(images/evening.jpg);";
}
?>


* {
  font-family: 'Oswald', sans-serif;
  margin: 0px;
  padding: 0px;
}

/**** HEADER
      header was axed in favor of the navbar!
****/
/*
header {
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  height: 17%;
  background-color: #7289DA;
  color: white;
}*/

/**** FOOTER ****/

footer {
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  bottom: 0px;
  left: 0px;
  right: 0px;
  height: 40px;
  font-size: 1rem;
  font-weight: lighter;
  color: white;
  background-color: #23272A;
}


/*********** NAV BAR **********/

nav {
  position: relative;
  top: 0px;
  left: 0px;
  right: 0px;

}

ul {
    list-style-type: none;
    margin:0px;
    height: 70px;
    font-size: 20px;
    line-height: 50px;
    overflow: hidden;
    background-color: #7289DA;

}

li {
  display: inline;



}

li a:hover {
    background-color: #23272A;
}

li a {
  font-size: 1.5rem;
  display: inline;
  color: white;
  text-align: center;
  padding: 12px 16px;
  text-decoration: none;
  background-color: #7289DA;
  -webkit-transition: 0.5s;
   transition: 0.5s;
}

li img {
  float: left;
  display: inline-block;
  color: white;


}

li h1 {
  float: left;
  color: white;
  display: inline-block;
  margin-top: 10px;
  margin-left: 10px;
  margin-right: 10px;

}

@media screen and (max-width: 600px){
    li {float: none;}
}

/****** KEYFRAMES ****/

@-webkit-keyframes fadeIn {
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
}
@keyframes fadeIn {
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
}
@-webkit-keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
            transform: translate3d(0, 100%, 0);
  }

  100% {
    opacity: 1;
    -webkit-transform: none;
            transform: none;
  }
}
@keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
            transform: translate3d(0, 100%, 0);
  }

  100% {
    opacity: 1;
    -webkit-transform: none;
            transform: none;
  }
}

@-webkit-keyframes fadeInRight {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
            transform: translate3d(100%, 0, 0);
  }

  100% {
    opacity: 1;
    -webkit-transform: none;
            transform: none;
  }
}
@keyframes fadeInRight {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
            transform: translate3d(100%, 0, 0);
  }

  100% {
    opacity: 1;
    -webkit-transform: none;
            transform: none;
  }
}

/****** INDEX.PHP *********/

#main_header {
  font-weight: lighter;
  font-size: 5rem;
  margin-bottom: 2rem;
  margin-top: -5rem;
  -webkit-animation-name: fadeInRight;
        animation-name: fadeInRight;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;
}

.greetings {
  z-index: 1;
  position: absolute;
  bottom: 40px;
  left: 30px;
  color: #23272A;
  font-size: 1.5rem;
  -webkit-animation-name: fadeInUp;
        animation-name: fadeInUp;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;

}

.display_main {
  position: absolute;
  top: 70px;
  width: 100%;
  bottom: 40px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #f2f2f2;

}

.display {
  position: absolute;
  top: 70px;
  width: 100%;
  bottom: 40px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

}

.imagesrow{
  display: flex;
  justify-content: space-around;
  width: 80%;
}

.imgcontainer1 {
  border-radius: 50%;
  border: 12px solid #99AAB5;
  width: 300px;
  height: 300px;
  background-image: url("images/mountains.jpg");
  background-position: center;
  background-size: cover;
  -webkit-animation-name: fadeIn;
        animation-name: fadeIn;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;
}
.imgcontainer2 {
  border-radius: 50%;
  border: 12px solid #99AAB5;
  width: 300px;
  height: 300px;
  background-image: url("images/city.jpg");
  background-position: center;
  background-size: cover;
  -webkit-animation-name: fadeIn;
  animation-name: fadeIn;
  -webkit-animation-duration: 0.5s;
  animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
  transition-timing-function: ease-in;
}
.imgcontainer3 {
  border-radius: 50%;
  border: 12px solid #99AAB5;
  width: 300px;
  height: 300px;
  background-image: url("images/beach.jpg");
  background-position: center;
  background-size: cover;
  -webkit-animation-name: fadeIn;
  animation-name: fadeIn;
  -webkit-animation-duration: 0.5s;
  animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
  transition-timing-function: ease-in;

}


/*********** FORMS *************/

.dlogo {

  -webkit-animation-name: fadeInRight;
        animation-name: fadeInRight;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;

}

.display_form {
  position: absolute;
  top: 100px;
  width: 100%;
  bottom: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  -webkit-animation-name: fadeIn;
        animation-name: fadeIn;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;


}

form {
  width: 75%;
}

select {
  outline: none;
  clear: both;
  box-sizing: border-box;
  width: 100%;
  padding: 5px;
  font-size: 1.1rem;
  border: none;
  border-bottom: 2px solid #c7c7c7;
}

input {
  outline: none;
  border: none;
  border-bottom: 2px solid #c7c7c7;
  box-sizing: border-box;
  width: 100%;
  padding: 8px;
  margin: 8px 0px;
  font-size: 1.1rem;
  -webkit-transition: 0.5s;
   transition: 0.5s;
}

input:focus {
  border: none;
  background-color: #d5d5d5;
  border-bottom: 2px solid #23272A;

}

textarea {
  outline: none;
  font-size: 1.1rem;
  width: 100%;
}

label {
  font-size: 1.1rem;
}

/******** BUTTONS ************/

.cssbutton {
  padding: 8px 15px;
  font-size: 28px;
  margin: 0;
  padding: 8px 15px;
  border-width: 0;
  border-color: transparent;
  background: transparent;
  font-weight: 400;
  cursor: pointer;
  position: relative;
  font-family: inherit;
  padding: 5px 12px;
  z-index: 0;
  border: none;
  background: #fff;
  color: #7289DA;
  -webkit-transition: all 0.3s cubic-bezier(0.02, 0.01, 0.47, 1);
  transition: all 0.3s cubic-bezier(0.02, 0.01, 0.47, 1);
  -webkit-animation-name: fadeInUp;
        animation-name: fadeInUp;
  -webkit-animation-duration: 0.5s;
          animation-duration: 0.5s;
  -webkit-transition-timing-function: ease-in;
          transition-timing-function: ease-in;
}

  .cssbutton:before {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #7289DA;
    content: '';
    opacity: 0;
    -webkit-transition: opacity 0.15s ease-out, -webkit-transform 0.15s ease-out;
    transition: opacity 0.15s ease-out, -webkit-transform 0.15s ease-out;
    transition: transform 0.15s ease-out, opacity 0.15s ease-out;
    transition: transform 0.15s ease-out, opacity 0.15s ease-out, -webkit-transform 0.15s ease-out;
    z-index: -1;
    -webkit-transform: scaleX(0);
            transform: scaleX(0);
}

.cssbutton:hover,
.cssbutton:focus {
  box-shadow: 0 1px 8px rgba(58,51,53,0.3);
  color: #fff;
  -webkit-transition: all 0.5s cubic-bezier(0.02, 0.01, 0.47, 1);
  transition: all 0.5s cubic-bezier(0.02, 0.01, 0.47, 1);
}

.cssbutton:hover:before,
.cssbutton:focus:before {
  opacity: 1;
  -webkit-transition: opacity 0.2s ease-in, -webkit-transform 0.2s ease-in;
  transition: opacity 0.2s ease-in, -webkit-transform 0.2s ease-in;
  transition: transform 0.2s ease-in, opacity 0.2s ease-in;
  transition: transform 0.2s ease-in, opacity 0.2s ease-in, -webkit-transform 0.2s ease-in;
  -webkit-transform: scaleX(1);
          transform: scaleX(1);
}

.cssbutton.danger {
  background: #ff5964;
  color: #fff;
}
.cssbutton.danger:hover,
.cssbutton.danger:focus {
  color: #fff;
}
