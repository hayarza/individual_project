<!-- Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS
UPDATE FORM

-->

<?php
  session_start();

  if ($_SESSION['loggedin'] == false) {
    header("Location: login.php");
    $_SESSION['returnpage'] = "update_form.php";
    $_SESSION["message"] = "You need to log in to view this page";
  }
 ?>
<?php
    include_once("variables.php");
    include_once("functions.php");
 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Agent Update Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="style.php" />
    <script src="script.js" type="text/javascript"></script>
    </head>

  <body>
    <?php include_once("navbar.php"); ?>
    <h1 style="text-align:center;">AGENT UPDATE FORM</h1>
    <div class="display_form" id="1">

      <form method="post" action="">
       <select name="AgentId" onchange="this.form.submit();">
         <option value="">Select Agent</option>
         <?php
         // -- SELECT DROPDOWN MENU --//

         $dbh = dbConnect();
         $query = "SELECT * FROM agents";
         $result = mysqli_query($dbh, $query);
         while ($row = mysqli_fetch_row($result))
                  {
                    echo "<option value ='$row[0]'>$row[1] $row[3]</option>";
                  }

          ?>
       </select>
      </form>

      <?php
      //-- SETTING SQL QUERY AND GETTING DATA FOR AGENT FORM --//
      if (!isset($_REQUEST['AgentId'])){

       echo "PICK AN AGENT";
       exit;

      } else {

       $dbh = dbConnect();
       $sql = "SELECT * FROM agents WHERE AgentId = $_REQUEST[AgentId]";
       $result = mysqli_query($dbh, $sql);
       if ($row = mysqli_fetch_assoc($result)) {

       // ---> GO TO FORM BELOW

       }
       else {
       echo "ERROR DB SERVER COULD BE DOWN" . mysqli_error($dbh);
      }
      }


      ?>

      <form method="post" action="updateagent.php" id="updateform">
        <label for="AgentId">Agent ID:</label>
        <input type="text" name="AgentId" value ="<?php echo $row['AgentId']; ?>" readonly="readonly" /> <br />
        <label for="AgtFirstName">First Name:</label>
        <input type="text" name="AgtFirstName" value ="<?php echo $row['AgtFirstName']; ?>"  /> <br />
        <label for="AgtMiddleInitial">Middle Initial:</label>
        <input type="text" name="AgtMiddleInitial" value ="<?php echo $row['AgtMiddleInitial']; ?>"  /> <br />
        <label for="AgtLastName">Last Name:</label>
        <input type="text" name="AgtLastName" value ="<?php echo $row['AgtLastName']; ?>"  /> <br />
        <label for="AgtBusPhone">Agent Phone Number:</label>
        <input type="text" name="AgtBusPhone" value ="<?php echo $row['AgtBusPhone']; ?>"  /> <br />
        <label for="AgtEmail">Email:</label>
        <input type="text" name="AgtEmail" value ="<?php echo $row['AgtEmail']; ?>"  /> <br />
        <label for="AgtPosition">Agent Position:</label>
        <input type="text" name="AgtPosition" value ="<?php echo $row['AgtPosition']; ?>"  /> <br />
        <label for="AgencyId">Agency ID:</label>
        <input type="text" name="AgencyId" value ="<?php echo $row['AgencyId']; ?>"\ /> <br />
        <div style="text-align:center;">
         <button class="cssbutton">UPDATE AGENT</button>
         <button type="reset" onclick="return confirm('Are you sure?');" class="cssbutton">RESET</button>
         <button onclick="changeAction();" class="cssbutton danger">DELETE AGENT</button>
        </div>
      </form>
    </div>


  </body>
<?php include_once("footer.php"); ?>
