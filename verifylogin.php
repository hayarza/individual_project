<!-- // Created by Hernan Ayarza on 5/22/17 for CPRG210 Web Application Concepts TRAVELEXPERTS // -->

<?php
  session_start();
  include_once("functions.php");


  if (!isset($_REQUEST['AgtUserName'])) {
      $_SESSION["message"] = "USERNAME and PASSWORD required!";
      header("Location: login.php");
  }
  $dbh = dbConnect();
  if (mysqli_connect_error()) {
    die("DB Connection Not Available");
  }
    $result = mysqli_query($dbh, "SELECT AgtPwd FROM agents WHERE AgtUserName='$_REQUEST[AgtUserName]'");
  if(!$result) {
    echo "Database Error:" . mysqli_error($dbh);
    exit();

  }

  $match = false;
  if ($pwd = mysqli_fetch_assoc($result)) {
    if ($pwd["AgtPwd"] == $_REQUEST["AgtPwd"]) {
      $match = true;
    }
  }
  if ($match) {
    $_SESSION["loggedin"] = true;
    if ($_SESSION['returnpage'] == "") {
      header("Location: dashboard.php");
    } elseif (isset($_SESSION['returnpage'])) {
      header("Location: $_SESSION[returnpage]");
      unset($_SESSION["returnpage"]);

  }
} else {
    $_SESSION["message"] = "USERNAME or PASSWORD INCORRECT";
    header("Location: login.php");
  }

 ?>
